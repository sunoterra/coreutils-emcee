function coreutils-emcee() {
  optBase="--classify --color --group-directories-first --human-readable"
  alias less="less --RAW-CONTROL-CHARS"
  # check for gnu coreutils
  if [ -x "/opt/local/bin/gls" ] || [ -x "/usr/local/bin/gls" ]
  then
    # gls nice-ness
    alias ll="gls ${optBase} -l "
    alias llr="gls ${optBase} --recursive -l "
    alias lr="gls ${optBase} --recursive "
    alias lra="gls ${optBase} --almost-all --recursive "
    alias ls="gls ${optBase} "
    alias lsa="gls ${optBase} --almost-all "
  else
    alias ll="ls -FGhl "
    alias llr="ls -FGRhl "
    alias lr="ls -FGRh "
    alias lra="ls -AFGRh "
    alias ls="ls -FGh "
    alias lsa="ls -AFGh "
  fi
}
coreutils-emcee
unset -f coreutils-emcee
